<?php
namespace Docs\MainBundle\DataProvider;

/**
 * Interface for all dataProviders
 *
 * Data Provider:
 *
 * internal data or
 * data coming from 3rd party APIs
 *
 * @author h.botev
 */
interface DataProviderInterface
{
}
