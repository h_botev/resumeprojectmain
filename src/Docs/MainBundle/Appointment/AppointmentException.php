<?php
namespace Docs\MainBundle\Appointment;

/**
 * Exception thrown when trying to
 * process appointment
 * @author hbotev
 */
class AppointmentException extends \Exception
{
}
