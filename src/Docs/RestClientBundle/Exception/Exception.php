<?php
namespace Docs\RestClientBundle\Exception;

/**
 * General exception thrown by RestClientBundle
 * 
 * @author h.botev
 *        
 */
class Exception extends \Exception
{
}
