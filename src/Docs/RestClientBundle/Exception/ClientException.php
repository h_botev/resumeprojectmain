<?php
namespace Docs\RestClientBundle\Exception;

/**
 * General exception thrown by the rest client instances
 * 
 * @author h.botev
 *        
 */
class ClientException extends Exception
{
}
