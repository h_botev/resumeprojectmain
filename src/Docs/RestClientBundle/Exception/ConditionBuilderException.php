<?php
namespace Docs\RestClientBundle\Exception;

/**
 * Exceptions thrown from the condition builder
 * 
 * @author h.botev
 *        
 */
class ConditionBuilderException extends Exception
{
}
